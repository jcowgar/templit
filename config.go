package templit

import (
	"os"
)

// Config options for a templit execution.
type Config struct {
	// TemplateDirName defines the path where all templates are stored.
	TemplateDirNames []string

	// OutputDirName is where generated files will be written.
	OutputDirName string

	// SystemVariables is all known system wide variables for template processing.
	SystemVariables Variables

	// DryRun causes the system to only determine what would be done, no files
	// are actually written.
	DryRun bool

	// Debug enables verbose output useful for debugging Templit and user
	// templates.
	Debug bool

	// Overwrite enables overwritting existing files instead of skipping if
	// they already exist.
	Overwrite bool

	// AllowShell enables the built in shell function in templates.
	//
	// It is disabled by default due to security concerns about a template
	// being able to execute any shell command.
	AllowShell bool
}

// BaseConfig creates a default configuration for templit execution.
func BaseConfig() Config {
	result := Config{
		SystemVariables: make(Variables),
	}

	// If we can determine the CWD, let's preconfigure the template directory
	// name for this instance. The user can override later if they wish.
	if cwd, err := os.Getwd(); err == nil {
		result.OutputDirName = cwd
	}

	return result
}

// AddTemplateDirHead adds a template to the top of the list. Templates are
// searched for from first in the array to last.
func (c *Config) AddTemplateDirHead(dir string) {
	c.TemplateDirNames = append([]string{dir}, c.TemplateDirNames...)
}

// AddTemplateDirTail adds a template to the bottom of the list. Templates are
// searched for from first in the array to last.
func (c *Config) AddTemplateDirTail(dir string) {
	c.TemplateDirNames = append(c.TemplateDirNames, dir)
}
