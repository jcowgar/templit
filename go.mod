module codeberg.org/jcowgar/templit

go 1.18

require github.com/ettle/strcase v0.1.1

require github.com/joho/godotenv v1.4.0

require (
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/aymerick/raymond v2.0.2+incompatible // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
