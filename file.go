package templit

import (
	"fmt"
	"os"
	"strings"

	"github.com/aymerick/raymond"
)

// readFile reads `filename` into a header and body variable.
func (t *Templit) readFile(filename string) (
	headerVars Variables,
	content string, err error,
) {
	raw, err := os.ReadFile(filename)
	if err != nil {
		return nil, "", fmt.Errorf("could not read file %s: %w", filename, err)
	}

	rawS := string(raw)

	parts := strings.SplitN(rawS, "---", 2)
	headerVars, err = t.parseHeaderVars(parts[0])
	if err != nil {
		return nil, "", fmt.Errorf("could not parse header: %w", err)
	}

	return headerVars, parts[1], nil
}

// parseHeaderVars uses the template system to parse the header of a template.
func (t *Templit) parseHeaderVars(header string) (Variables, error) {
	ctx := t.Config.SystemVariables
	parsedHeader, err := raymond.Render(header, ctx)
	if err != nil {
		return nil, fmt.Errorf("could not process header: %w", err)
	}

	headers := strings.Split(parsedHeader, "\n")
	headerVars := make(map[string]string)

	for _, line := range headers {
		if line == "" {
			continue
		}
		if t.Config.Debug {
			fmt.Printf("processing line: %s\n", line)
		}
		parts := strings.SplitN(line, ":", 2)
		if t.Config.Debug {
			fmt.Printf("parts = %v\n", parts)
		}
		headerVars[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
	}

	return headerVars, nil
}

func fileExists(filename string) bool {
	_, err := os.Stat(filename)

	return err == nil
}
