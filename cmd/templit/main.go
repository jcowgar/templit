package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"strings"

	templit "codeberg.org/jcowgar/templit"
	"github.com/adrg/xdg"
	"github.com/joho/godotenv"
)

var (
	scmrev  = "unknown"
	version = "unknown"
)

func usage() {
	fmt.Printf("templit version %s (%s)\n", version, scmrev)
	fmt.Println()
	fmt.Printf("usage: %s template-name [flags]\n", os.Args[0])
	fmt.Println()

	flag.PrintDefaults()
}

func main() {
	var (
		showHelp    bool
		templateDir string
		outputDir   string
	)

	_ = godotenv.Load()

	cfg := templit.BaseConfig()

	flag.Usage = usage
	flag.StringVar(
		&outputDir,
		"output-dir",
		"",
		"set the output directory, defaults to .",
	)
	flag.StringVar(
		&templateDir,
		"template-dir",
		"",
		"set the template directory, defaults to .templits",
	)
	flag.BoolVar(&cfg.AllowShell, "allow-shell", false, "allow shell command in templates")
	flag.BoolVar(&cfg.DryRun, "dry-run", false, "show all actions but perform none")
	flag.BoolVar(&cfg.Overwrite, "overwrite", false, "overwrite generated files")
	flag.BoolVar(&cfg.Debug, "debug", false, "turn on debug output")
	flag.BoolVar(&showHelp, "help", false, "show this help message")
	flag.Parse()

	if len(flag.Args()) == 0 {
		fmt.Println("error: template-name must be supplied.")
		fmt.Println()

		usage()

		os.Exit(1)
	}

	for _, argVal := range flag.Args()[1:] {
		parts := strings.SplitN(argVal, "=", 2)
		cfg.SystemVariables[parts[0]] = parts[1]
	}

	// Setup the template directories
	//
	// Command Line Param
	// Up Dir Tree
	// ENV Var
	// Xdg Config
	//
	cwd, _ := os.Getwd()

	if templateDir != "" {
		cfg.AddTemplateDirTail(templateDir)
	}

	if cwd != "" {
		templitInPath := findRootTemplitDir(cwd)
		if templitInPath != "" {
			cfg.AddTemplateDirTail(templitInPath)
		}
	}

	templitDirEnv := os.Getenv("TEMPLIT_DIR")
	if templitDirEnv != "" {
		cfg.AddTemplateDirTail(templitDirEnv)
	}

	if xdg.ConfigHome != "" {
		templitHomeConfigDir := path.Join(xdg.ConfigHome, "templit")
		if fileExists(templitHomeConfigDir) {
			cfg.AddTemplateDirTail(templitHomeConfigDir)
		}
	}

	// Allow overriding the default output directory by command line
	if outputDir != "" {
		cfg.OutputDirName = outputDir
	} else {
		cfg.OutputDirName = cwd
	}

	t, err := templit.New(cfg, flag.Arg(0))
	if err != nil {
		fmt.Printf("error: %s\n", err)
		fmt.Println()

		usage()

		os.Exit(1)
	}

	t.Process()

	if t.Config.DryRun {
		fmt.Println()
		fmt.Println("dry run complete, no files written")
	}
}

func findRootTemplitDir(startDir string) string {
	paths := strings.Split(startDir, string(os.PathSeparator))
	paths[0] = string(os.PathSeparator)

	for i := range paths {
		parts := append(paths[:len(paths)-i], ".templits")
		path := path.Join(parts...)

		if fileExists(path) {
			return path
		}
	}

	return ""
}

func fileExists(filename string) bool {
	_, err := os.Stat(filename)

	return err == nil
}
