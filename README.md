# Templit

Templit is a project template tool. You can create any file and structure
you wish and create new items from that.

Templit is written in Go and uses the Handlebars template syntax. For detail
instructions and examples on the syntax of Handlebars, please see
[Handlebars Introduction](https://handlebarsjs.com/guide/).

## Installation

For now, you must have Go installed:

```
$ go install codeberg.org/jcowgar/templit/cmd/templit@latest
```

## Basics

Templit looks for a master template directory in either the environment
variable `TEMPLIT_DIR`, the command line option `-template-dir` or `.templits`
in the current working directory.

The master template directory contains sub directories, of which are the names
for templates you can invoke. Under these sub directories you can have any
structure you wish. Templit will recreate that structure in your output
directory.

Each file found in the master template's sub directory will be recreated using
the template found.

## Simple Example

Given the directory structure:

```
project-dir/
  .templits/
    controller/
	  controller.js
	  tests/test.js
	migration/
	  migrations/
	    up.sql
		  down.sql
```

You have two "master" templates you can invoke. 'controller' and 'migration'. Following are the content of each file. These are simple examples, not meant to be real (or working) JavaScript/SQL.

```
# controller/controller.js
filename: {{ lowercase(snakecase name) }}_controller.js
---
// {{ camelcase name }}Controller is a controller.
class {{ pascalcase name }}Controller {
{{#each (split routeNames ",") }}
  function handle{{ gocase . }} {
    console.log("Route {{ gocase . }} is not yet implemented")
  }
{{/each}}
}

# controller/tests/test.js
filename: {{ lowercase(snakecase name) }}_test.js
---
class {{ name }}Test {
}


# migrations/up.sql
filename: {{ now "060102150405" }}-{{ lowercase(kebabcase name) }}.up.sql
---
-- See .templits/config.yaml file for author and email variables
-- UP MIGRATION by {{ author }} <{{ email }}>

BEGIN;

-- UP MIGRATION CONTENT HERE

COMMIT;

# migrations/down.sql
filename: {{ now "060102150405" }}-{{ lowercase(kebabcase name) }}.down.sql
---

-- DOWN MIGRATION

BEGIN;

-- DOWN MIGRATION CONTENT HERE

COMMIT;
```

Now, in your project directory, you can:

Generate a few new controllers:

```
$ templit controller name=Ping routeNames=ping,pong
$ templit controller name=SayHello routeNames=hi,hello,howdy,bye,cya
```

This would in turn create with the appropriate content:

```
ping_controller.js
say_hello_controller.js
tests/
  ping_controller_test.js
  say_hello_controller_test.js
```

If you needed to create a new SQL migration:

```
$ templit migration name="Create new users table"
```

This would in turn create with the appropriate content:

```
migrations/
  22070312332943-create-new-users-table.up.sql
  22070312332943-create-new-users-table.down.sql
```

## Use

Any name/value pair can be supplied on the command line after any options.

```
templit myCoolTemplate name=HelloWorld count=5 people=John,Jack,Joe
```

In your template, you can access `{{ name }}`, `{{ count }}` and
`{{ people }}`. In addition to these, all environment variables are
exposed to templates.

You can use functions in your templates such as:

```
{{#each (split people ",")}}
  echo "Hello, {{ . }}"
{{/each}}
```

There are "modifier" type functions:

- lowercase lower
- uppercase UPPER
- snakecase snake_case
- kebabcase kebab-case
- camelcase camelCase
- pascalcase PascalCase
- gocase HTTPHandler

You can access the current date via the `now` function which takes a format as it's parameter.

You can split a string by some delimiter via the `split` function.

If you have TEMPLIT_ALLOW_SHELL=Y or use the -allow-shell parameter, you can use the `shell`
function to include the standard output of any shell command, such as {{ shell
"git rev-parse head" }}.

Filenames will be used as is unless there is a "header" block in your template that redefines
the filename to use, as seen in the above examples.

## Getting Help

templit is still very and will likely change. Most changes, however, will
probably be enhancements. The core templating engine is using Handlebars, so it
is stable and mature.

If you need help, I would suggest opening an issue here on
[codeberg.org/jcowgar/templit](https://codeberg.org/jcowgar/templit/issues).

## Helping

PRs are **always** welcome for new features and bug fixes.
