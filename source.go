package templit

import (
	"fmt"
	"path"
	"strings"
)

type Source struct {
	SourceFilename      string
	DestinationFilename string
	HeaderVariables     Variables
	Content             string
}

func ParseSource(t *Templit, srcFname string) (Source, error) {
	var destFname string
	fmt.Printf("   parsing: %s\n", srcFname)

	destDirName, _ := path.Split(srcFname)
	destDirName = strings.TrimPrefix(destDirName, t.templateDir)
	destDirName = path.Join(t.Config.OutputDirName, destDirName)

	headerVars, content, err := t.readFile(srcFname)
	if err != nil {
		return Source{}, fmt.Errorf("could not read template %s: %w", srcFname, err)
	}

	if headerFilename, ok := headerVars["filename"]; ok {
		destFname = path.Join(destDirName, headerFilename)
	} else {
		tmpFname := strings.TrimPrefix(srcFname, t.templateDir)
		destFname = path.Join(destDirName, tmpFname)
	}

	fmt.Printf("will write: %s\n", destFname)

	return Source{
		SourceFilename:      srcFname,
		DestinationFilename: destFname,
		HeaderVariables:     headerVars,
		Content:             content,
	}, nil
}
