package templit

import (
	"fmt"
	"os"
	"path"

	"github.com/aymerick/raymond"
)

// Process will generate all templates in `templateName` substituting
// all values for `vars`.
func (t *Templit) Process() error {
	for _, s := range t.Templates {
		err := s.Process(t)
		if err != nil {
			return fmt.Errorf("could not process template: %w", err)
		}
	}

	return nil
}

func (s *Source) Process(t *Templit) error {
	fmt.Printf("processing: %s\n", s.DestinationFilename)
	if fileExists(s.DestinationFilename) {
		if t.Config.Overwrite != true {
			fmt.Println("      skip: generated file exists")
			return nil
		}

		fmt.Println("      warn: file exists, overwritting")
	}

	if t.Config.DryRun {
		return nil
	}

	destDir, _ := path.Split(s.DestinationFilename)
	os.MkdirAll(destDir, 0o750)

	fh, err := os.Create(s.DestinationFilename)
	if err != nil {
		panic(fmt.Errorf("could not create %s: %w", s.DestinationFilename, err))
	}
	defer fh.Close()

	ctx := t.Config.SystemVariables
	result, err := raymond.Render(s.Content, ctx)
	if err != nil {
		panic(fmt.Errorf("could not render template %s: %w", s.DestinationFilename, err))
	}

	err = os.WriteFile(s.DestinationFilename, []byte(result), 0o660)
	if err != nil {
		panic(fmt.Errorf("could not generate %s: %w", s.DestinationFilename, err))
	}

	return nil
}
