package templit

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"os/exec"
	"strings"
	"time"

	"github.com/aymerick/raymond"
	"github.com/ettle/strcase"
)

func setupHelpers() {
	raymond.RegisterHelper("lowercase", strings.ToLower)
	raymond.RegisterHelper("uppercase", strings.ToUpper)
	raymond.RegisterHelper("snakecase", strcase.ToSnake)
	raymond.RegisterHelper("kebabcase", strcase.ToKebab)
	raymond.RegisterHelper("camelcase", strcase.ToCamel)
	raymond.RegisterHelper("pascalcase", strcase.ToPascal)
	raymond.RegisterHelper("gocase", strcase.ToGoPascal)
	raymond.RegisterHelper("now", formatNow)
	raymond.RegisterHelper("replacechars", replaceChars)
	raymond.RegisterHelper("split", split)
	raymond.RegisterHelper("csv", parseCsv)
}

func setupShellHelper() {
	raymond.RegisterHelper("shell", shellCommand)
}

func formatNow(format string) string {
	now := time.Now()

	return now.Format(format)
}

func replaceChars(chars string, with string, in string) string {
	replacements := []string{}

	for _, v := range strings.Split(chars, "") {
		replacements = append(replacements, v, with)
	}

	replacer := strings.NewReplacer(replacements...)

	return replacer.Replace(in)
}

func shellCommand(cmd string) string {
	var stdOut bytes.Buffer
	var stdErr bytes.Buffer

	c := exec.Command("sh", "-c", cmd)
	c.Stdout = &stdOut
	c.Stderr = &stdErr

	err := c.Run()
	if err != nil {
		fmt.Println("StdOut")
		fmt.Println(stdOut.String())
		fmt.Println("StdErr")
		fmt.Println(stdErr.String())

		panic(err)
	}

	return strings.TrimSpace(stdOut.String())
}

func split(value string, delim string) []string {
	delim = strings.ReplaceAll(delim, "\\n", "\n")

	return strings.Split(value, delim)
}

func parseCsv(value string) [][]string {
	sreader := strings.NewReader(value)
	reader := csv.NewReader(sreader)

	records, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}

	return records
}
