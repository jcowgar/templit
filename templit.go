package templit

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"gopkg.in/yaml.v3"
)

// Templit represents configuration for a templit execution.
type Templit struct {
	// MasterTemplate that will be processed.
	MasterTemplate string

	// Config for templit execution.
	Config Config

	// Templates is an array of all found templates in a given master template.
	Templates []Source

	// templateDir is the base directory where templates are to be searched for.
	templateDir string
}

// New constructs a Templit structure.
func New(cfg Config, masterName string) (Templit, error) {
	vars := make(map[string]string)
	for _, v := range os.Environ() {
		parts := strings.SplitN(v, "=", 2)
		vars[parts[0]] = parts[1]
	}

	if cfg.AllowShell || os.Getenv("TEMPLIT_ALLOW_SHELL") == "Y" {
		setupShellHelper()
	}
	setupHelpers()

	result := Templit{
		MasterTemplate: masterName,
		Config:         cfg,
	}

	for _, templitDir := range cfg.TemplateDirNames {
		d := path.Join(templitDir, masterName)
		if fileExists(d) {
			result.templateDir = d
			break
		}
	}

	if result.templateDir == "" {
		return Templit{}, fmt.Errorf("could not find master template %s", masterName)
	}

	err := result.loadConfigFiles()
	if err != nil {
		return Templit{}, err
	}

	result.scanDirectory(result.templateDir)

	return result, nil
}

// loadConfigFiles will load configuration from each templit directory
// defined. The top most will take priority.
func (t *Templit) loadConfigFiles() error {
	for _, templateDir := range t.Config.TemplateDirNames {
		for _, fname := range []string{"config.yaml", "config.yml"} {
			if err := t.LoadConfigFile(path.Join(templateDir, fname)); err != nil &&
				err != os.ErrNotExist {
				return fmt.Errorf("could not load configuration file: %w", err)
			}
		}
	}

	return nil
}

// LoadConfigFile loads a Templit configuration file.
func (t *Templit) LoadConfigFile(configFilename string) error {
	if !fileExists(configFilename) {
		return os.ErrNotExist
	}

	var cfg templitConfig
	data, err := ioutil.ReadFile(configFilename)
	if err != nil {
		return fmt.Errorf("could not read %s: %w", configFilename, err)
	}

	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		return fmt.Errorf("could not parse YAML: %w", err)
	}

	for k, v := range cfg.Variables {
		if _, ok := t.Config.SystemVariables[k]; !ok {
			t.Config.SystemVariables[k] = string(v)
		}
	}

	return nil
}

func (t *Templit) scanDirectory(dir string) error {
	fileInfo, err := ioutil.ReadDir(dir)
	if err != nil {
		return fmt.Errorf("could not scan template directory '%s': %w", dir, err)
	}

	for _, fi := range fileInfo {
		if fi.IsDir() {
			subDirName := path.Join(dir, fi.Name())
			err = t.scanDirectory(subDirName)
			if err != nil {
				return err
			}
		} else {
			srcFilename := path.Join(dir, fi.Name())

			src, err := ParseSource(t, srcFilename)
			if err != nil {
				return err
			}

			t.AddSource(src)
		}
	}

	return nil
}

// AddSource adds a template to be processed.
func (t *Templit) AddSource(s Source) {
	t.Templates = append(t.Templates, s)
}
