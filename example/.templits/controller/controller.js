filename: {{ lowercase(snakecase name) }}_controller.js
---
// {{ camelcase name }}Controller is a controller.
class {{ pascalcase name }}Controller {
{{#each (split routeNames ",") }}
	function handle{{ gocase . }} {
		console.log("Route {{ gocase . }} is not yet implemented")
	}
{{/each}}
}
