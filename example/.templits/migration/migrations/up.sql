filename: {{ now "060102150405" }}-{{ lowercase(kebabcase name) }}.up.sql
---
-- See .templits/config.yaml file for author and email variables
-- UP MIGRATION by {{ author }} <{{ email }}>

BEGIN;

-- UP MIGRATION CONTENT HERE

COMMIT;