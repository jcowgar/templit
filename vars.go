package templit

import (
	"fmt"
)

// Variables represents variable substitutions used in the template processing.
type Variables map[string]string

func printVars(vars Variables) {
	for k, v := range vars {
		fmt.Printf("k=%s, v=%s\n", k, v)
	}
}
